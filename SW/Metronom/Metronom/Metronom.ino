/*
 Name:		Metronom.ino
 Created:	1/11/2018 7:59:36 PM
 Author:	zoli
*/

#include <Arduino.h>
#include "TM16XX.h"
#include "Segment7.h"

#define PERIOD_MIN 50
#define PERIOD_MAX 200
#define MIN_IN_MS 60000
#define KEY_UP B11101111
#define KEY_DOWN B01101111
#define KEY_TIME_MS 200

#define LEDTIME MIN_IN_MS/250

const byte PIN_CLK = 2;   // define CLK pin (any digital pin)
const byte PIN_DIO = 0;   // define DIO pin (any digital pin)
TM16XX display(TM1637, PIN_CLK, PIN_DIO);

uint8_t period = PERIOD_MIN;
uint16_t period_ms;
// uint16_t LED[4] = { 0x0101, 0x0202, 0x0204, 0x0208 };
// uint16_t LED[4] = { 0x0101, 0x0202, 0x0204, 0x0208 };
uint16_t LED[4] = { 0x0108, 0x0304, 0x0302, 0x0201 };

uint8_t ClearLed[3] = { 0,0,0 };

uint32_t millis_period_initial = 0;
uint32_t millis_period_end = 0;
uint8_t period_counter = 0;

uint8_t keys;
bool enable_up = true;
bool enable_down = true;
uint32_t millis_up;
uint32_t millis_down;
uint8_t speed_key = 0;

// the setup function runs once when you press reset or power the board
void setup()
{
	// startup delay (maybe lowered when we see the production hardware behaviour
	delay(200);

	// Set display brightness (change to adaptive in the next version)
	display.SetDisplayState(true, TM16XX_CONTROL_PWM_14);
	// Clear screen
	display.Clear();
	// initial number display
	display.DisplayNum(3, false, 3, period);

	period_ms = MIN_IN_MS / period;

	// 0 - Blue
	// 1 - Red
	// 2 - Green
}

// the loop function runs over and over again until power down or reset
void loop()
{
	// switch the leds off after 1/250 min
	
	if (millis() >= millis_period_initial + LEDTIME)
	{
		display.DisplayBin(0, ClearLed, 3);
	}
	

	// Set the LEDs
	if (millis() >= millis_period_end)
	{
		// save the value imediatelly as strict timing is required
		millis_period_initial = millis();
		millis_period_end = millis_period_initial + period_ms;
		display.DisplayBin(0, ClearLed, 3);
		// display.DisplayBin(LED[period_counter] >> 8, LED[period_counter] & 0x00FF);
		for (uint8_t i = 0; i < 3; i++)
		{
			if (LED[period_counter] & (0x0100 << i))
			{
				display.DisplayBin(i + 1, LED[period_counter] & 0x00FF);
			}
		}
		period_counter = period_counter > 2 ? 0 : period_counter + 1;
	}


	keys = display.p2_read();
	// Key and display control
	switch (keys)
	{
		case KEY_UP:
			if (period < PERIOD_MAX && enable_up)
			{
				if (speed_key >= 5)
				{
					period += 10;
					if (period > PERIOD_MAX)
					{
						period = PERIOD_MAX;
					}

				}
				else
				{
					period++;
					speed_key++;
				}
				period_ms = MIN_IN_MS / period;
				display.DisplayBin(3, ClearLed, 3);
				display.DisplayNum(3, false, 3, period);
				enable_up = false;
				millis_up = millis() + KEY_TIME_MS;
			}
			break;
		case KEY_DOWN:
			if (period > PERIOD_MIN && enable_down)
			{
				if (speed_key >= 5)
				{
					period -= 10;
					if (period < PERIOD_MIN)
					{
						period = PERIOD_MIN;
					}

				}
				else
				{
					period--;
					speed_key++;
				}
				period_ms = MIN_IN_MS / period;
				display.DisplayBin(3, ClearLed, 3);
				display.DisplayNum(3, false, 3, period);
				enable_down = false;
				millis_down = millis() + KEY_TIME_MS;
			}
			break;
		default:
			speed_key = 0;
	}
	enable_up = millis() >= millis_up;
	enable_down = millis() >= millis_down;
}

