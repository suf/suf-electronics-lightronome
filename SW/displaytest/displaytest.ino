
#include "SevenSegmentTM1637.h"
// #include "SevenSegmentExtended.h"

const byte PIN_CLK = 2;   // define CLK pin (any digital pin)
const byte PIN_DIO = 3;   // define DIO pin (any digital pin)
SevenSegmentTM1637      display(PIN_CLK, PIN_DIO);

void setup() {
  // put your setup code here, to run once:
  display.begin(); 
  display.setBacklight(100);
  delay(1000);
  display.write("123456");
}

void loop() {
  // put your main code here, to run repeatedly:

}
