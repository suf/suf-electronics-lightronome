$fn=100;
use <rachet.scad>

rod_dia=25;

difference()
{
    union()
    {
        translate([-10,0,0])
            cube([20,37+rod_dia,18]);
        translate([0,23+rod_dia/2,0])
            cylinder(d=rod_dia+11,h=18);
    }
    translate([-5,-0.001,-0.001])
        cube([10,21,20.002]);
    translate([-1,-0.001,-0.001])
        cube([2,25,20.002]);
    translate([0,23+rod_dia/2,-0.001])
        cylinder(d=rod_dia+1,h=20.002);
    translate([0,19+rod_dia,9])
        rotate([-90,30,0])
        {
            cylinder(d=11.3,h=10,$fn=6);
            cylinder(d=7,h=20);
        }
    translate([-10.001,9,9])
        rotate([0,90,0])
            cylinder(d=6.5,h=20.002);

}
for(i=[-1,1])
translate([-7 * i,9,9])
    rotate([0,90 * i,0])
        rachet(18);
