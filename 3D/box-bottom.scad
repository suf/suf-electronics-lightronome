$fn=100;

include <board.scad>
include <5050.scad>

module bottom()
{

// bottom: 2.5
// board bottom: 5 (M4 nut must fit)
// board: 1.6
// USB conn: 2.7

    difference()
    {
        union()
        {
            difference()
            {
                minkowski()
                {
                    cube([60,33.5,6]);
                    cylinder(r=8,h=6);
                }
                translate([0,0,2.5])
                    minkowski()
                    {
                        cube([60,33.5,6]);
                        cylinder(r=5.5,h=6);
                    }
                translate([0,0,10])
                    minkowski()
                    {
                        cube([60,33.5,6]);
                        cylinder(r=7,h=6);
                    }
            }
            for(i=[0,60])
                for(j=[0,22])
                    translate([i,j,2.5])
                        cylinder(d=8,h=5);
            translate([20,17,2.5])
                cylinder(d=10,h=5);
            translate([40,17,2.5])
                cylinder(d=10,h=5);
            // USB
            translate([-8,2,8.6])
                cube([2.5,20,3.4]);

            // Jack Shield
            translate([65.5,5,3.6])
                cube([2.5,12,10]);
        }
        for(i=[0,60])
            for(j=[0,22])
                translate([i,j,-0.001])
                {
                    cylinder(d=4,h=7.502);
                    cylinder(d1=7,d2=0.001,h=3.5);
                }
        translate([20,17,-0.001])
            cylinder(d=4.5,h=7.502);
        translate([20,17,3.5])
            cylinder(d=8,h=4.02,$fn=6);

        translate([40,17,-0.001])
            cylinder(d=4.5,h=7.502);
        translate([40,17,3.5])
            cylinder(d=8,h=4.02,$fn=6);
        
        // ISP
        translate([4.5,33,8.6])
            cube([8.62,15,6.04]);
        // USB
        translate([-10,7.5,8.6])
            cube([6,9,3.7]);
        translate([-8.001,4,5.6])
            cube([1.5,16,6.7]);
        // Jack
        translate([65,11,13.6])
            rotate([0,90,0])
                cylinder(d=7,h=3.001);

    }
}

bottom();
