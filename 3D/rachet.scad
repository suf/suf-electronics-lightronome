$fn=100;

module rachet(dia)
{
    difference()
    {
        cylinder(d=dia + 0.001,h=4);
        for(j=[0:23])
        {
            rotate([0,0,7.5+15*j])
                hull()
                {
                    translate([0,0,4.001])
                        sphere(d=0.001,$fn=1);
                    translate([0,dia/2 + 1,2.5])
                        sphere(d=0.001,$fn=1);
                    for(i=[-1,1])
                    rotate([0,0,7.5*i])
                        translate([0,dia/2 + 1,4.001])
                            sphere(d=0.001,$fn=1);
                }
        }
        translate([0,0,2.5])
            cylinder(d=10,h=2.501);
        translate([0,0,-0.001])
            cylinder(d=6.5,h=2.502);
    }
    
}

rachet(30);