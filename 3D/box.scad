$fn=100;

include <board.scad>
include <5050.scad>
use <box-top.scad>
use <box-bottom.scad>
use <box-mount.scad>

/*
translate([0,0,7.5])
    board();
*/

color("gray",0.8)
    bottom();

translate([0,0,30])
    board();

/*
translate([0,53,0])
    top();
*/
color("gray",0.8)
    translate([60,0,60])
        rotate([0,180,0])
            top();
/*
translate([60,0,22.229])
    rotate([0,180,0])
        top();
*/
/*
difference()
{
translate([60,0,22.229])
    rotate([0,180,0])
        top();
translate([-20,-20,-0.001])
    cube([100,20,30]);
}
*/

color("gray",0.8)
    translate([30,17,-30])
        rotate([0,180,0])
            box_mount();