$fn=200;

difference()
{
    union()
    {
        difference()
        {
            cylinder(d=30, h=20);
            translate([0,0,2.5])
                cylinder(d=25, h=17.501);
            translate([0,0,-0.001])
                cylinder(d=6.5, h=2.502);
        }
        for(i=[-1,1])
        {
            translate([i*11,0,0])
                cylinder(d=8, h=20);
        }

    }
    for(i=[-1,1])
    {
        translate([i*11,0,-0.001])
        {
            cylinder(d=3.5, h=20.002);
            cylinder(d1=7,d2=0.001, h=3.5);
        }
    }
    
}