$fn=100;

include <board.scad>
include <5050.scad>

module top()
{
    difference()
    {
        union()
        {
            difference()
            {
                union()
                {
                    minkowski()
                    {
                        cube([60,33.5,5.1]);
                        cylinder(r=8,h=5.1);
                    }
                    translate([0,0,9.7])
                        minkowski()
                        {
                            cube([60,33.5,1]);
                            cylinder(r=6.5,h=1);
                        }
                }
                translate([0,0,2.5])
                    minkowski()
                    {
                        cube([60,33.5,6]);
                        cylinder(r=5.5,h=6);
                    }
            }
            for(i=[0,60])
                for(j=[0,22])
                    translate([i,j,2.5])
                        cylinder(d=6,h=10.6);
            // LED Reflector
            for(i=[0:3])
            {
                hull()
                {
                    translate([i*20,29,10.999])
                        cylinder(d=7,h=0.001);
                    translate([i*20,33,0])
                        cylinder(d=17,h=0.001);
                }
            }
            // Button
            translate([51,-0.65,0])
                hull()
                    for(i=[0:2])
                        rotate([0,0,i*120])
                            translate([0,4.5,0])
                                cylinder(d=3,h=8.6);
            translate([37,0.65,0])
                hull()
                    for(i=[0:2])
                        rotate([0,0,i*120])
                            translate([0,-4.5,0])
                                cylinder(d=3,h=8.6);
        }
        for(i=[0,60])
            for(j=[0,22])
                translate([i,j,2.5])
                    cylinder(d=2.5,h=10.601);

        // ISP
        translate([55.5-8.62,33,7.5])
            cube([8.62,15,6.04]);
        // Jack Shield
        translate([-8.001,4.5,8.6])
            cube([2.502,13,10]);
        translate([-8.001,11,8.6])
            rotate([0,90,0])
                cylinder(d=7,h=3.002);
        // USB
        translate([65.499,1.5,10.2])
            cube([6,21,3.7]);
        translate([65.499,7.5,9.7])
            cube([6,9,3.7]);
        translate([66.501,4,6.6])
            cube([1.5,16,6.7]);
        // Display
        translate([10.5,4.5,-0.001])
            cube([39,20,2.502]);
        // LEDS
            for(i=[0:3])
            {
                hull()
                {
                    translate([i*20,29,11])
                        cylinder(d=4,h=0.001);
                    translate([i*20,33,-0.001])
                        cylinder(d=14,h=0.001);
                }
            }
        // Button
        translate([51,-0.65,0])
            hull()
                for(i=[0:2])
                    rotate([0,0,i*120])
                        translate([0,2.5,-0.001])
                            cylinder(d=1.5,h=8.602);
        translate([37,0.65,0])
            hull()
                for(i=[0:2])
                    rotate([0,0,i*120])
                        translate([0,-2.5,-0.001])
                            cylinder(d=1.5,h=8.602);
    }
}

top();
