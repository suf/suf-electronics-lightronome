$fn=100;

use <rachet.scad>

module box_mount()
{
    translate([0,0,20])
        for(i=[-1,1])
            rotate([0,90*i,0])
                rachet(20);


    difference()
    {
        cylinder(d=34,h=2.5);
        for(i=[-10,10])
            translate([i,0,-0.001])
                cylinder(d=4.5,h=2.502);
    }

    difference()
    {
        translate([-4,-10,0])
            cube([8,20,20]);
        translate([-4.001,0,20])
            rotate([0,90,0])
                cylinder(d=20,h=8.002);
    }

    difference()
    {
        translate([-8,-14,2.5])
            cube([16,28,4]);
        for(i=[-1,1])
            translate([8*i,-14.001,6.5])
                rotate([-90,0,0])
                    cylinder(d=8.002,h=28.002);
        for(i=[-1,1])
            translate([8,14.001*i,6.5])
                rotate([0,-90,0])
                    cylinder(d=8.002,h=16.002);
        for(i=[-10,10])
            translate([i,0,-0.001])
                cylinder(d=11,h=6.502);
    }
}

box_mount();