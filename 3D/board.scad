$fn=100;

include <5050.scad>

module display()
{
    color("gray")
        cube([38,19,8.2]);
}

module tackt()
{
    color("silver")
        translate([-2.5,-2.5,0])
            cube([5,5,1]);
    color("silver")
        cylinder(d=3.8,h=1.25);
    color("gold")
        cylinder(d=2,h=1.5);
}

module pin_header_double_90(pin_num)
{
    for(i=[0:pin_num-1])
    {
        translate([2.54*i,0,0])
        {
            union()
            {
                color("black")
                {
                    intersection()
                    {
                        cube([2.54,2.54,2.54]);
                        translate([1.27,-0.001,1.27])
                            rotate([0,45,0])
                                translate([-1.5,-0.001,-1.5])
                                    cube([3,2.542,3]);
                    }
                    intersection()
                    {
                        translate([0,0,2.54])
                            cube([2.54,2.54,2.54]);
                        translate([1.27,-0.001,3.81])
                            rotate([0,45,0])
                                translate([-1.5,-0.001,-1.5])
                                    cube([3,2.542,3]);
                    }
                }
                color("gold")    
                {
                    translate([1.27-0.32,-0.82,1.27-0.32])
                        cube([0.64,9.36,0.64]);
                    translate([1.27-0.32,-0.82-2.54,3.81-0.32])
                        cube([0.64,9.36+2.54,0.64]);
                    translate([1.27-0.32,-1.82,-3.6])
                        cube([0.64,0.64,4.19]);
                    translate([1.27-0.32,-1.82-2.54,-3.6])
                        cube([0.64,0.64,4.19+2.54]);
                    translate([1.59,-0.82,0.59])
                        rotate([90,0,-90])
                            difference()
                            {
                                intersection()
                                {
                                    cylinder(r=1,h=0.64);
                                    translate([0,0,-0.001])
                                        cube([1.501,1.501,0.642]);
                                }
                                translate([0,0,-0.001])
                                    cylinder(r=0.36,h=0.642);
                            }
                    translate([1.59,-0.82-2.54,0.59+2.54])
                        rotate([90,0,-90])
                            difference()
                            {
                                intersection()
                                {
                                    cylinder(r=1,h=0.64);
                                    translate([0,0,-0.001])
                                        cube([1.501,1.501,0.642]);
                                }
                                translate([0,0,-0.001])
                                    cylinder(r=0.36,h=0.642);
                            }
                }
            }
        }
    }
}

module board()
{
    // board

    difference()
    {
        color("green")
            minkowski()
            {
                cube([60,30,0.8]);
                cylinder(d=10,h=0.8);
            }
        for(i=[0,60])
            for(j=[0,22])
                translate([i,j,-0.001])
                    cylinder(d=3.5,h=1.602);
    }
    translate([11,5,6.6])
        display();

    for(i=[0:3])
        translate([i*20,29,1.6])
            rotate([0,0,90])
                LED5050();

    for(i=[0:3])
        translate([9+ i*14,0,1.6])
            tackt();

    // USB

        color("silver")
            translate([-6,8,1.6])
                cube([6,8,2.7]);

    // Jack
        color("black")
            translate([53,5,1.6])
                cube([12,12,9]);

        color("silver")
            translate([65,11,6.1])
                rotate([0,90,0])
                    cylinder(d=6,h=3);

    // ISP
    translate([5,33,1.6])
        pin_header_double_90(3);
}